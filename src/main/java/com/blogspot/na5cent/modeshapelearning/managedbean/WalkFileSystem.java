/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import com.blogspot.na5cent.modeshapelearning.walker.FolderWalker;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jittagornp
 */
public class WalkFileSystem {
    
    private static final Logger LOG = LoggerFactory.getLogger(WalkFileSystem.class);
    
    public static void main(String[] args) {
        new FolderWalker(new File("D:\\my-project")) {
            @Override
            public void before(File current) {
                LOG.debug("Path => {}, File name => {}", current.getParent().replace("\\", "/").replace("D:", ""), current.getName().replace("\\", "/").replace("D:", ""));
            }
            
            @Override
            public void after(File current) {
                
            }
        }.start();
    }
}
