/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import com.blogspot.na5cent.modeshapelearning.util.jcr.JCRNodeUtils;
import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFNotification;
import com.blogspot.na5cent.modeshapelearning.util.jcr.JCRPropertiesUtils;
import java.io.Serializable;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jittagornp
 */
@Component
@RequestScoped
public class FindByPathManagedBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(FindByPathManagedBean.class);
    //
    private String path;
    private Node resultNode;
    //
    @Autowired
    private Repository repository;

    @PostConstruct
    public void postConstruct() {
    }

    public void onFind() {
        if (path == null) {
            path = "";
        }

        path = path.trim();

        Session session = null;
        try {
            session = repository.login("default");
            resultNode = session.getNode(path);
            JCRNodeUtils.showNodeInformation(resultNode);
            showProperties(resultNode);
        } catch (LoginException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } finally {
            if (session != null) {
                session.logout();
            }
        }
    }

    private void showProperties(Node node) {
        try {
            PropertyIterator propertyIterator = node.getProperties();
            while (propertyIterator.hasNext()) {
                Property property = propertyIterator.nextProperty();
                LOG.debug("property => {}", JCRPropertiesUtils.getPropertyValue(property));
            }
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        }
    }

    public Node getResultNode() {
        return resultNode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
