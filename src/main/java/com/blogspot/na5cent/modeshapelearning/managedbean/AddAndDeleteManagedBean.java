/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import com.blogspot.na5cent.modeshapelearning.util.jcr.JCRNodeUtils;
import com.blogspot.na5cent.modeshapelearning.util.jcr.JCRPropertiesUtils;
import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFNotification;
import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jittagornp
 */
@Component
@RequestScoped
public class AddAndDeleteManagedBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AddAndDeleteManagedBean.class);
    //
    private DefaultTreeNode rootTree;
    private DefaultTreeNode selectedNode;
    private List<Property> properties;
    private String parentPath;
    private String childName;
    //
    @Autowired
    private Repository repository;

    @PostConstruct
    public void postConstruct() {
        rootTree = new DefaultTreeNode();
        properties = new ArrayList<Property>();
    }

    public void onAdd() {

        Session session = null;
        try {
            session = repository.login("default");
            Node node = session.getNode(parentPath);
            node.addNode(childName, "nt:folder");

            session.save();
            if (rootTree != null) {
                rootTree.getChildren().clear();
            }
            walkNode(session.getRootNode(), rootTree);
        } catch (LoginException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } finally {
            if (session != null) {
                //session.logout();
            }
        }
    }

    public void listNode() {
        Session session = null;
        try {
            session = repository.login("default");
            walkNode(session.getRootNode(), rootTree);
        } catch (LoginException ex) {
            LOG.warn(null, ex);
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        } finally {
            if (session != null) {
                //session.logout();
            }
        }
    }

    private void walkNode(Node node, DefaultTreeNode parentTree) throws RepositoryException {
        LOG.debug("node name => {}", node.getName());
        //
        parentTree.setData(node);
        parentTree.setExpanded(true);
        NodeIterator nodeIterator = node.getNodes();
        while (nodeIterator.hasNext()) {
            DefaultTreeNode treeNode = new DefaultTreeNode();
            treeNode.setParent(parentTree);
            walkNode(nodeIterator.nextNode(), treeNode);
        }
    }

    public void onDelete() {
        String deletePath = JSFRequest.requestString("deletePath");
        LOG.debug("deletePath => {}", deletePath);
        Session session = null;
        try {
            session = repository.login("default");
            Node node = session.getNode(deletePath);
            node.remove();
            session.save();
            if (rootTree != null) {
                rootTree.getChildren().clear();
            }

            walkNode(session.getRootNode(), rootTree);
        } catch (LoginException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } finally {
            if (session != null) {
                //session.logout();
            }
            
            selectedNode = null;
        }
    }

    public DefaultTreeNode getRootTree() {
        return rootTree;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public DefaultTreeNode getSelectedNode() {
        return selectedNode;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        try {
            selectedNode = (DefaultTreeNode) event.getTreeNode();
            Node node = (Node) selectedNode.getData();
            JCRPropertiesUtils.showProperties(node);
            JCRNodeUtils.showNodeInformation(node);
            properties.clear();
            PropertyIterator propertyIterator = node.getProperties();
            if (propertyIterator != null) {
                while (propertyIterator.hasNext()) {
                    Property property = propertyIterator.nextProperty();
                    properties.add(property);
                }
            }
        } catch (Exception ex) {
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        }
    }

    public List<Property> getProperties() {
        return properties;
    }
}
