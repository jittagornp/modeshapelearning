/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.jcr.Binary;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jittagornp
 */
@Component
@RequestScoped
public class DownloadFileManagedBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DownloadFileManagedBean.class);
    @Autowired
    private Repository repository;
    private StreamedContent file;
    private List<Node> nodes;

    @PostConstruct
    public void postConstruct() {
        nodes = new ArrayList<Node>();
        Session session = null;
        try {
            session = repository.login("default");
            Node uploadFolder = session.getNode("/test/upload");
            NodeIterator nodeIterator = uploadFolder.getNodes();
            while (nodeIterator.hasNext()) {
                nodes.add(nodeIterator.nextNode());
            }
        } catch (LoginException ex) {
            LOG.warn(null, ex);
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        } finally {
            if (session != null) {
                //session.logout();
            }
        }
    }

    public StreamedContent getFile() {
        return file;
    }

    public void onDownloadFile() {
        
        String fileName = JSFRequest.requestString("fileName");
        Session session = null;
        try {
            session = repository.login("default");
            Node uploadFolder = session.getNode("/test/upload");
            Node binaryFile = uploadFolder.getNode(fileName).getNode("jcr:content");
            Binary binary = binaryFile.getProperty("jcr:data").getBinary();
            String mimeType = binaryFile.getProperty("jcr:mimeType").getString();
            InputStream inputStream = binary.getStream();
            LOG.debug("fileName => {}", fileName);
            LOG.debug("mimeType => {}", mimeType);
            LOG.debug("inputStream => {}", inputStream);
            file = new DefaultStreamedContent(binary.getStream(), mimeType, fileName);  
        } catch (LoginException ex) {
            LOG.warn(null, ex);
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        } finally {
            if (session != null) {
                //session.logout();
            }
        }
    }

    public List<Node> getNodes() {
        return nodes;
    }
}
