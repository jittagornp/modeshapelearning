/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Level;
import javax.faces.bean.SessionScoped;
import javax.jcr.Binary;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author redcrow
 */
@Component
@SessionScoped
public class UploadFileManagedBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UploadFileManagedBean.class);
    @Autowired
    private Repository repository;

    public void onUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        Session session = null;
        try {
            Calendar lastModified = Calendar.getInstance();
            session = repository.login("default");
            Node uploadFolder = session.getNode("/test/upload");
            Node fileNode = uploadFolder.addNode(file.getFileName(), "nt:file");
            Node contentNode = fileNode.addNode("jcr:content", "nt:resource");
            Binary binary = session.getValueFactory().createBinary(file.getInputstream());
            contentNode.setProperty("jcr:data", binary);
            contentNode.setProperty("jcr:lastModified", lastModified);
            
            session.save();
        } catch (LoginException ex) {
            LOG.warn(null, ex);
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        } catch (IOException ex) {
            LOG.warn(null, ex);
        } finally {
            if (session != null) {
                //session.logout();
            }
        }
    }
}
