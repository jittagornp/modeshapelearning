/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.managedbean;

import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFNotification;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jittagornp
 */
@Component
@SessionScoped
public class SearchManagedBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(SearchManagedBean.class);
    //
    private String expression;
    private List<Node> resultNodes;
    private static final String QUERY_LANGUAGE = Query.JCR_SQL2;
    //
    @Autowired
    private Repository repository;

    @PostConstruct
    public void postConstruct() {
        resultNodes = new ArrayList<Node>();
    }

    public void onSearch() {
        if(expression == null){
            expression = "";
        }
        
        resultNodes.clear();
        expression = expression.trim();
        Session session = null;
        try {
            session = repository.login("default");
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            Query query = queryManager.createQuery(expression, QUERY_LANGUAGE);
            QueryResult result = query.execute();
            
            NodeIterator nodeIterator = result.getNodes();
            while (nodeIterator.hasNext()) {
                resultNodes.add(nodeIterator.nextNode());
            }
        } catch (LoginException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
            JSFNotification.notifyClient(JSFNotification.ERROR, "Exception", ex.getMessage());
        } finally {
            if (session != null) {
                session.logout();
            }
        }
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<Node> getResultNodes() {
        return resultNodes;
    }
}
