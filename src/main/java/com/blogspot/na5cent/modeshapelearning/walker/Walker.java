/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.walker;

/**
 *
 * @author jittagornp
 */
public abstract class Walker<T> {

    protected T root;

    public abstract void before(T current);

    public abstract void after(T current);

    protected abstract void listAll(T parent);

    public void start() {
        listAll(root);
    }
}
