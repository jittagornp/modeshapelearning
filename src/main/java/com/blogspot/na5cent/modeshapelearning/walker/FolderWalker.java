/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.walker;

import java.io.File;

/**
 *
 * @author jittagornp
 */
public abstract class FolderWalker extends Walker<File> {

    private FolderWalker() {
    }

    public FolderWalker(File rootFile) {
        this.root = rootFile;
    }

    @Override
    protected void listAll(File parent) {
        File[] listFiles = parent.listFiles();
        if (listFiles == null) {
            return;
        }

        for (File file : listFiles) {
            before(file);
            listAll(file);
            after(file);
        }
    }
}
