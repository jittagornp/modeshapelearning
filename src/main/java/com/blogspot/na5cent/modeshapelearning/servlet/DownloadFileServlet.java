/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.servlet;

import com.blogspot.na5cent.modeshapelearning.util.jsf.JSFSpringUtils;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.jcr.Binary;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jittagornp
 */
@WebServlet(urlPatterns = "/downloadFile")
public class DownloadFileServlet extends HttpServlet {
    
    private static final Logger LOG = LoggerFactory.getLogger(DownloadFileServlet.class);
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        //
        Session session = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            Repository repository = JSFSpringUtils.getBean(getServletContext(), Repository.class);
            session = repository.login("default");
            outputStream = response.getOutputStream();
            
            Node uploadFolder = session.getNode("/test/upload");
            Node binaryFile = uploadFolder.getNode(fileName).getNode("jcr:content");
            Binary binary = binaryFile.getProperty("jcr:data").getBinary();
            String mimeType = binaryFile.getProperty("jcr:mimeType").getString();
            inputStream = binary.getStream();
            
            LOG.debug("fileName => {}", fileName);
            LOG.debug("mimeType => {}", mimeType);
            LOG.debug("inputStream => {}", inputStream);
            
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setContentLength((int) binary.getSize());
            
            ByteStreams.copy(inputStream, outputStream);
        } catch (LoginException ex) {
            LOG.warn(null, ex);
        } catch (NoSuchWorkspaceException ex) {
            LOG.warn(null, ex);
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        } finally {
            if (session != null) {
                session.logout();
            }
            
            if(inputStream != null){
                inputStream.close();
            }
            
            if(outputStream != null){
                outputStream.close();
            }
        }
    }
}
