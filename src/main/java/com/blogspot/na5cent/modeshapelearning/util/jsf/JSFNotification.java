/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.util.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 *
 * @author jittagornp
 */
public class JSFNotification {

    public static final Severity INFO = FacesMessage.SEVERITY_INFO;
    public static final Severity ERROR = FacesMessage.SEVERITY_ERROR;
    public static final Severity FATAL = FacesMessage.SEVERITY_FATAL;
    public static final Severity WARN = FacesMessage.SEVERITY_WARN;

    public static void notifyClient(FacesMessage.Severity notifyType, String title, String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(notifyType, title, message));
    }
}
