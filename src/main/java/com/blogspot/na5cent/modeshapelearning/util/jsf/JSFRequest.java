/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.util.jsf;

import javax.faces.context.FacesContext;

/**
 *
 * @author jittagornp
 */
public class JSFRequest {

    public static Integer requestInteger(String parameterName) {
        String request = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get(parameterName);

        if (request == null) {
            return null;
        }
        
        return Integer.valueOf(request);
    }

    public static String requestString(String parameterName) {
        return FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get(parameterName);
    }
}
