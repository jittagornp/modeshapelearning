/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.util.jcr;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jittagornp
 */
public class JCRNodeUtils {
    
    private static final Logger LOG = LoggerFactory.getLogger(JCRNodeUtils.class);
    
    public static void showNodeInformation(Node node) throws RepositoryException{
        //LOG.debug("node.getAllowedLifecycleTransistions().toString() => {}", node.getAllowedLifecycleTransistions().toString());
        LOG.debug("node.getCorrespondingNodePath(\"default\") => {}", node.getCorrespondingNodePath("default"));
        NodeDefinition definition = node.getDefinition();
        //
        LOG.debug("node.getDefinition().allowsSameNameSiblings() => {}", definition.allowsSameNameSiblings());
        LOG.debug("node.getDefinition().getDeclaringNodeType().getName() => {}", definition.getDeclaringNodeType().getName());
        LOG.debug("node.getDefinition().getDefaultPrimaryTypeName() => {}", definition.getDefaultPrimaryTypeName());
        
        LOG.debug("node.getDepth() => {}", node.getDepth());
        LOG.debug("node.getIdentifier() => {}", node.getIdentifier());
        LOG.debug("node.getIndex() => {}", node.getIndex());
        LOG.debug("node.getMixinNodeTypes().toString() => {}", node.getMixinNodeTypes().toString());
        LOG.debug("node.getName() => {}", node.getName());
        LOG.debug("node.getPath() => {}", node.getPath());
        LOG.debug("node.getPrimaryNodeType().getName() => {}", node.getPrimaryNodeType().getName());
        LOG.debug("node.getSession().getUserID() => {}", node.getSession().getUserID());
    }
}
