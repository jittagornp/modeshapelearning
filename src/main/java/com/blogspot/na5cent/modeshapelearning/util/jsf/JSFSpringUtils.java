/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.util.jsf;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.jsf.FacesContextUtils;

/**
 *
 * @author jittagornp
 */
public class JSFSpringUtils {

    public static <T> T getBean(Class<T> clazz) {
        ServletContext servletContext = FacesContextUtils
               .getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
               .getServletContext();
        return WebApplicationContextUtils
               .getWebApplicationContext(servletContext)
               .getBean(clazz);
    }

    public static Object getBean(String bean) {
        ServletContext servletContext = FacesContextUtils
               .getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
               .getServletContext();
        return WebApplicationContextUtils
               .getWebApplicationContext(servletContext)
               .getBean(bean);
    }

    public static <T> T getBean(ServletContext servletContext, Class<T> clazz) {
        return WebApplicationContextUtils
               .getWebApplicationContext(servletContext)
               .getBean(clazz);
    }

    public static Object getBean(ServletContext servletContext, String bean) {
        return WebApplicationContextUtils
               .getWebApplicationContext(servletContext)
               .getBean(bean);
    }
}
