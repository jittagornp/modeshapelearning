/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.modeshapelearning.util.jcr;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jittagornp
 */
public class JCRPropertiesUtils {

    private static final Logger LOG = LoggerFactory.getLogger(JCRPropertiesUtils.class);

    public static String getPropertyValue(Property property) throws RepositoryException {
        String value = "";
        switch (property.getType()) {
            case PropertyType.STRING:
                value = "String : " + property.getString();
                break;
            case PropertyType.BINARY:
                value = "Bianry : " + property.getBinary() + "";
                break;
            case PropertyType.DATE:
                value = "Date : " + property.getDate() + "";
                break;
            case PropertyType.DOUBLE:
                value = "Double : " + property.getDouble() + "";
                break;
            case PropertyType.LONG:
                value = "Long : " + property.getLong() + "";
                break;
            case PropertyType.BOOLEAN:
                value = "Boolean : " + property.getBoolean() + "";
                break;
            case PropertyType.NAME:
                value = "Name : " + property.getName() + "";
                break;
            case PropertyType.PATH:
                value = "Path : " + property.getPath() + "";
                break;
            case PropertyType.REFERENCE:
                value = "Reference : ";//property.getNode().getReferences().;
                break;
            case PropertyType.WEAKREFERENCE:
                value = "Weak Reference : ";
                break;
            case PropertyType.URI:
                value = "Url : ";
                break;
        }

        return value;
    }

    public static void showProperties(Node node) {
        try {
            PropertyIterator propertyIterator = node.getProperties();
            while (propertyIterator.hasNext()) {
                Property property = propertyIterator.nextProperty();
                LOG.debug("property => {}", JCRPropertiesUtils.getPropertyValue(property));
            }
        } catch (RepositoryException ex) {
            LOG.warn(null, ex);
        }
    }
}
