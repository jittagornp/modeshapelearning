package com.blogspot.na5cent.modeshapelearning.jcr;

import javax.transaction.TransactionManager;
import org.infinispan.transaction.lookup.TransactionManagerLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.jta.JtaTransactionManager;

/**
 * {@link TransactionManagerLookup} implementation needed to make Infinispan use
 * a custom transaction manager. Even though normally Infinispan will create a
 * new instance of this class, we use a static member and control bean
 * initialization order to make sure that Atomikos transaction manager has been
 * injected.
 *
 * @author Horia Chiorean (hchiorea@redhat.com)
 */
public class PlatFormTransactionManagerLookup implements TransactionManagerLookup {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlatFormTransactionManagerLookup.class);
    @Autowired
    private JtaTransactionManager transactionManager;

    @Override
    public TransactionManager getTransactionManager() {
        if (transactionManager != null) {
            return transactionManager.getTransactionManager();
        } else {
            return null;
        }
    }
}
